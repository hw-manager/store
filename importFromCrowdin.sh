# !/bin/bash
# By Nico Alt
# See "LICENSE" for license information

# Download and unzip files
mkdir tmp
cd tmp
wget https://crowdin.com/download/project/hw-manager.zip
unzip hw-manager.zip

# Delete files in store directory
rm -R ../description-{ar,cs,de,en,es,fa,fr,hu}
rm -R ../short_description-{ar,cs,de,en,es,fa,fr,hu}

# Copy downloaded files to store directory
cp -R description-ar-rSA ../description-ar
cp -R description-cs-rCZ ../description-cs
cp -R description-de-rDE ../description-de
cp -R description-en-rUS ../description-en
cp -R description-es-rES ../description-es
cp -R description-fa-rIR ../description-fa
cp -R description-fr-rFR ../description-fr
cp -R description-hu-rHU ../description-hu

# Copy downloaded files to store directory
cp -R short_description-ar-rSA ../short_description-ar
cp -R short_description-cs-rCZ ../short_description-cs
cp -R short_description-de-rDE ../short_description-de
cp -R short_description-en-rUS ../short_description-en
cp -R short_description-es-rES ../short_description-es
cp -R short_description-fa-rIR ../short_description-fa
cp -R short_description-fr-rFR ../short_description-fr
cp -R short_description-hu-rHU ../short_description-hu

# Delete temporary directory
cd ..
rm -R tmp

# Show changes
git status
